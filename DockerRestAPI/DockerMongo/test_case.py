import acp_times
import nose
import arrow

# Testing the opening time
def test_open():

    beginTime = arrow.now()
    assert acp_times.open_time(300, 400, beginTime.isoformat()) == beginTime.shift(hours =+ 9, minutes =+ 0).isoformat()

# Testing the closing time
def test_close():

    beginTime = arrow.now()
    assert acp_times.close_time(200, 400, beginTime.isoformat()) == beginTime.shift(hours =+ 13, minutes =+ 20).isoformat()

# Testing the final checkpoint closing time in brevert distnation
def test_final_dist_close_time():

    beginTime = arrow.now()
    assert acp_times.close_time(200, 200, beginTime.isoformat()) == beginTime.shift(hours =+ 13, minutes =+ 30).isoformat()

# Testing the beginning checkpoint closing time if it was equel to 1
def test_beginning_close_time():

    beginTime = arrow.now()
    assert acp_times.close_time(0, 200, beginTime.isoformat()) == beginTime.shift(hours =+ 1, minutes =+ 0).isoformat()

# Testing the 20% range cases for checkpoints above the brevet by 20%    
def test_20_percent_case_close_time():

    beginTime = arrow.now()
    assert acp_times.close_time(480, 400, beginTime.isoformat()) == beginTime.shift(hours =+ 27, minutes =+ 0).isoformat()

# Testing the checkpoints above 20% range and it should be returned as the begining time and it is an error handler
def test_above_20_percent_case():

    beginTime = arrow.now()
    assert acp_times.close_time(490, 400, beginTime.isoformat()) == beginTime.shift(hours =+ 0, minutes =+ 0).isoformat()

# Testing a wrong answer that does not follow the acp rules
# The answer should be a fixed 40 hours added, but here
# we have it being calculated instead. 
def test_wrong_acp_rule():

    beginTime = arrow.now()
    assert acp_times.close_time(720, 600, beginTime.isoformat()) != beginTime.shift(hours =+ 50, minutes =+ 30).isoformat()
