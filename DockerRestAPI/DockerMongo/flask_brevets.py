"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import pymongo
import os
from pymongo import MongoClient
import logging

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    # Got these vars from the html file
    brevetDist = request.args.get('brevetDist', 999, type=float)
    beginDate = request.args.get('beginDate', 999, type=str)
    beginTime = request.args.get('beginTime', 999, type=str)
    #proper time format
    timeFormat = "{}T{}".format(beginDate, beginTime)
    app.logger.debug("{}T{}".format(beginDate, beginTime))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXED: These probably are the right open and close times
    open_time = acp_times.open_time(km, brevetDist, arrow.get(timeFormat).isoformat())
    close_time = acp_times.close_time(km, brevetDist, arrow.get(timeFormat).isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


# Pushing the data to MongoDB
@app.route('/new', methods=['POST'])
def new():
    # Getting the data from the calculator
    beginDate = request.form['begin_date']
    beginTime = request.form['begin_time']
    # Used getlist so i get all the data in the table
    km = request.form.getlist('km') 
    open_time = request.form.getlist('open')
    close_time = request.form.getlist('close')

    # Creating empty lists for data appending
    km_list = []
    open_time_list = []
    close_time_list = []

    # getting the control distance
    for data in km:
        # Checking if the field is empty so it stop appending
        if str(data) != '':
            
            km_list.append(str(data))
            
    # Getting the openning time             
    for data in open_time:
        # Checking if the field is empty so it stop appending            
        if str(data) != '':
                        
            open_time_list.append(str(data))
            
    # Getting the closing time
    for data in close_time:
        # Checking if the field is empty so it stop appending                    
        if str(data) != '':
                                
            close_time_list.append(str(data))
            
    # Deleting the old data from database         
    db.tododb.delete_many({})
    # Sending the data to the database 
    for i in range(len(km_list)):
        
        item_doc = {
            'beginDate': beginDate,
            'beginTime': beginTime,
            'km': km_list[i],
            'open_time': open_time_list[i],
            'close_time': close_time_list[i]
        }
        # Adding the data to database
        db.tododb.insert_one(item_doc)

    #Refreshing the page after clicking submit
    return redirect(url_for("index"))
    
# Displaying the data in a new page
@app.route('/display', methods=['POST'])
def display():
    # Loading the data from the database
    _items = db.tododb.find()
    items = [item for item in _items]

    # Checking if there is data in the database or not
    if items == []:
        return redirect(url_for("index"))
    else:
        return render_template('todo.html',items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
