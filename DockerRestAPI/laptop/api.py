# Laptop Service
import pymongo
import os
import csv
import sys
from pymongo import MongoClient
from flask import Flask, request, Response
from flask_restful import Resource, Api

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Getting the database
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class listAll(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]
            
        # A list to append the data from MongoDB
        open_time_list = []
        close_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])
            close_time_list.append(data['close_time'])

        # Returning the final results    
        return {
            'open_time': open_time_list,
            'close_time': close_time_list
        }
    
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll')

class listOpenOnly(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
            
        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB    
        open_time_list = []

        # Appending the data to the lists 
        for data in items:
            open_time_list.append(data['open_time'])
                        
        # Returning the final results 
        return {
            'open_time': open_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listOpenOnly, '/listOpenOnly')

class listCloseOnly(Resource):
    def get(self):
	# Getting the top var from the URI
        top_nums = request.args.get('top')
	# if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        close_time_list = []
	
        # Appending the data to the lists
        for data in items:
            close_time_list.append(data['open_time'])

        # Returning the final results 
        return {
            'close_time': close_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listCloseOnly, '/listCloseOnly')


class listAllJSON(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        open_time_list = []
        close_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])
            close_time_list.append(data['close_time'])

        # Returning the final results
        return {
            'open_time': open_time_list,
            'close_time': close_time_list
        }

# Create routes
# Another way, without decorators
api.add_resource(listAllJSON, '/listAll/json')

class listOpenOnlyJSON(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        open_time_list = []

        # Appending the data to the lists
        for data in items:
            open_time_list.append(data['open_time'])

        # Returning the final results
        return {
            'open_time': open_time_list,
        }

# Create routes
# Another way, without decorators 
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')


class listCloseOnlyJSON(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]

        # Else, if Var top is in URI
        else:
            top_nums = int(top_nums)
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        # A list to append the data from MongoDB
        close_time_list = []
        # Appending the data to the lists
        for data in items:
            close_time_list.append(data['open_time'])

        # Returning the final results
        
        return {
            'close_time': close_time_list,
        }

# Create routes
# Another way, without decorators
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')

class listAllCSV(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Open_Time', 'Close_Time'])

        for data in items:
            csv_writer.writerow([data['open_time'], data['close_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')
    
        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators
api.add_resource(listAllCSV, '/listAll/csv')

class listOpenOnlyCSV(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Open_Time'])

        for data in items:
            csv_writer.writerow([data['open_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')

        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators 
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')

class listCloseOnlyCSV(Resource):
    def get(self):
        # Getting the top var from the URI
        top_nums = request.args.get('top')
        # if the var top was not in URI
        if top_nums == None:
            # Getting the data from MongoDB
            _items = db.tododb.find()
            items = [item for item in _items]
        # Else, if Var top is in URI
        else:
            # Saving the num in an integer form
            top_nums = int(top_nums)
            # Getting and sorting data and limiting the number of data requested from MongoDB
            _items = db.tododb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top_nums)
            items = [item for item in _items]

        csvfile = open('data.csv', 'w')
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Close_Time'])

        for data in items:
            csv_writer.writerow([data['close_time']])
        # Returning the final results
        csvfile = open('data.csv', 'r')

        return Response(csvfile, mimetype='text/csv')

# Create routes
# Another way, without decorators 
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
BB
