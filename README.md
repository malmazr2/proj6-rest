# Project 6: Brevet time calculator service
# Majed Almazrouei - malmazr2@uoregon.edu

----------------------------------
# Very Important to run the program.
# PUT the creditinial file inside DockerRestAPI/DockerMongo
# Use the run shell in the DockerRestAPI folder "$ bash run.sh "

#The project is about listing service from project 5 stored in MongoDB database using API rest.
# Also designed a consumer page using php.

# Ports to access the servers
    * localhost:5003 - access the ACP calculator page
    * localhost:5001 - access the APIs pages	
    * localhost:5000 - access the php website


# Functionality i added: 

    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* Designed two different representations: one in csv and one in json.
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Added a query parameter to get top "k" open and close times.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format
